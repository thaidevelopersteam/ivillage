//
//  ViewController.swift
//  iVillage
//
//  Created by Nattapol Chittrichat on 11/19/2559 BE.
//  Copyright © 2559 Nattapol Chittrichat. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    @IBOutlet weak var tableView: UITableView!
    
    var tableData:Array< datastruct > = Array < datastruct >()

    struct datastruct
    {
        var id:String?
        var title:String?
        var detail:String?
        var image:String?
        var price:String?

        init(item: NSDictionary)
        {
            id = item["id"] as? String
            title = item["title"] as? String
            detail = item["detail"] as? String
            price = item["price"] as? String
            image = item["image"] as? String
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.

        let requestURL: NSURL = NSURL(string: "http://www.thaidevelopers.com/workshop/product.php")!
        let urlRequest: NSMutableURLRequest = NSMutableURLRequest(url: requestURL as URL)
        let session = URLSession.shared
        let task = session.dataTask(with: urlRequest as URLRequest) {
            (data, response, error) -> Void in
            
            let httpResponse = response as! HTTPURLResponse
            let statusCode = httpResponse.statusCode
            
            if (statusCode == 200) {
                print("ดึงข้อมูลสินค้าสำเร็จ")
                do{
                    do {
                        let resultsDict = try JSONSerialization.jsonObject(with: data!, options:.allowFragments) as! Dictionary<String, Any>
                        
                        // Get all product.
                        let items: Array<Dictionary<String, String>> = resultsDict["product"] as! Array<Dictionary<String, String>>

                        for i in 0 ..< items.count {
                            var item = Dictionary<String, String>()
                            
                            item["id"] = items[i]["id"]
                            item["title"] = items[i]["title"]
                            item["detail"] = items[i]["detail"]
                            item["price"] = items[i]["price"]
                            item["image"] = items[i]["image"]
                            
                            self.tableData.append(datastruct(item: item as NSDictionary))
                            
                        }
                        
                    }
                    
                } catch {
                    print("ดึงข้อมูลสินค้าไม่สำเร็จ เพราะ:\(error)")
                }
              
                self.tableView.reloadData()
                
            }
            
            DispatchQueue.main.async(execute: {
                
                return
            })
            
        }
        
        task.resume()
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.tableData.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "productCell", for: indexPath)
        
        let item = self.tableData[indexPath.row]
        
        cell.textLabel?.text = item.title
        cell.detailTextLabel?.text = item.detail
        
        let url = URL(string: item.image!)
        do {
            let imageData = try Data.init(contentsOf: url!)
            cell.imageView?.image = UIImage(data: imageData)
            
        } catch {
           print("เกิดข้อผิดพลาดขณะดาวน์โหลดภาพ \(error)")
        }
        
        return cell
        
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier! == "segueDetail")
        {
            
            let nextViewController = segue.destination as? DetailViewController
            let i = self.tableView.indexPathForSelectedRow?.row
            
            let item = self.tableData[i!]
            nextViewController?.datastruct = item
            
        } else {
            
        }
    }
    
}

